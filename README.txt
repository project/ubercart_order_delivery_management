Order deliver management module is helps to assign site orders to the list of available admin created vendors based on the delivery locations.

Intall:

Installing this module depend the ubercart contributed module. Once the module get install, it will create a vacabulary "Add UC Vendor Detais" to add the vendor inforamtion by admin.

If the module install after lot of orders created, it will pull all the orders which are in "pending" status in to custom table.

While creating the order this module allows to insert an entry of new order to custom table.

Whenever the order assign to vendor and different order status changes on the order deliver management form, it will update the order status on ubercart order status and send mail notification to the vendor about list of orders assigned to him.

If the admin change the order status it will get update in custom table as well.
